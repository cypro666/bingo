package container

import (
	"strings"
	"testing"
)

func TestCounter(t *testing.T) {
	var s = MakeSet4sFrom(strings.Split("ABC", ""))
	s.Add("key")

	if !(s.Contain("key")) {
		t.Fail()
	}

	if !(len(s) == 4) {
		t.Fail()
	}

	s.Remove("key")

	if !(!s.Contain("key")) {
		t.Fail()
	}

	if !(len(s) == 3) {
		t.Fail()
	}

	s.Clear()
	if !(len(s) == 0) {
		t.Fail()
	}

	str := "ZZZZZZZZZAABBCCCDDDDEEEEEFFFFFFGGGGGGGHHHHHHHH12345"
	var c = MakeCounter4s(100)

	for i := 0; i < len(str); i++ {
		c[string(str[i])] += 1
	}

	t.Logf("%v\n", c.MostMommon(4))
	t.Logf("%v\n", c.SortByValue()[:4])

	s1 := make(Set)
	s2 := make(Set)
	s3 := make(Set)
	s4 := make(Set)

	for i := 10; i < 21; i++ {
		s1.Add((i))
	}
	for i := 12; i < 24; i++ {
		s2.Add((i))
	}
	for i := 14; i < 27; i++ {
		s3.Add((i))
	}
	for i := 16; i < 30; i++ {
		s4.Add((i))
	}

	u := SetUnion(s4, s2, s3, s1)
	items := u.GetItems()

	t.Logf("%v\n", items)

	if len(u) != 20 {
		t.Fail()
	}

	items = SetIntersection(s1, s3, s2, s4).GetItems()
	t.Logf("%v\n", items)

	if len(items) != 5 {
		t.Fail()
	}

	items = SetDifference(s1, s2, s3, s4).GetItems()
	t.Logf("%v\n", items)

	if len(items) != 15 {
		t.Fail()
	}

	ret := SetUnion(s1)
	if ret != nil {
		t.Fail()
	}

}

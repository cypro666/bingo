// Experimental
package container

type Parent struct {
	ID   string
	Name string
}

type ParentTree map[string]*Parent

func NewParentTree() *ParentTree {
	return &ParentTree{}
}

func (tree *ParentTree) Clear() {
	*tree = make(map[string]*Parent)
}

func (tree ParentTree) Exist(id string) bool {
	_, exist := tree[id]
	return exist
}

func (tree ParentTree) Find(id string) (parent *Parent, exist bool) {
	parent, exist = tree[id]
	return
}

func (tree ParentTree) FindRoot(id string, root string) (parent *Parent, exist bool) {
	parent, exist = tree[id]

	if !exist {
		return
	}

	if parent.Name == root {
		return
	} else {
		return tree.FindRoot(parent.ID, root)
	}

}

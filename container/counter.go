// Helper for make counter-like ADTs
// Support two key types: string and interface
package container

import (
	"container/heap"
	"sort"
)

// Counter for strings or Keys can be cast to strings
type Counter4s map[string]int

// Make new Counter4s, length is the initialized size of map
func MakeCounter4s(length int) Counter4s {
	return make(Counter4s, length)
}

// Get Item4sList of Counter4s without sorting
func (c4s Counter4s) GetItems() Item4sList {
	seq := make(Item4sList, len(c4s))
	idx := 0

	for k, v := range c4s {
		seq[idx] = Item4s{k, v}
		idx += 1
	}

	return seq
}

// Sort the Counter4s by counted values in descend order
func (c4s Counter4s) SortByValue() Item4sList {
	seq := c4s.GetItems()
	sort.Sort(seq)
	return seq
}

// Sort the Counter4s by counted values in ascend order
func (c4s Counter4s) SortByValueAscend() Item4sList {
	seq := c4s.GetItems()
	sort.Sort(sort.Reverse(seq))
	return seq
}

// Item of Counter4s
type Item4s struct {
	Key string
	Val int
}

// For collect all items of Counter4s
type Item4sList []Item4s

// Implement of sort interfaces for Item4sList
// Note the default orderer is descend, means it's Great not Less
func (list Item4sList) Less(i, j int) bool {
	return list[i].Val > list[j].Val
}

// Implement of sort interfaces for Item4sList
func (list Item4sList) Len() int {
	return len(list)
}

// Implement of sort interfaces for Item4sList
func (list Item4sList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}

// Heap interface implements for most common method
func (list *Item4sList) Push(value interface{}) {
	*list = append(*list, value.(Item4s))
}

// Heap interface implements for most common method
func (list *Item4sList) Pop() interface{} {
	old := *list
	num := len(old)
	ret := old[num-1]
	*list = old[0 : num-1]
	return ret
}

// An optimized most common method for large counter
func (c4s Counter4s) MostMommon(topN int) Item4sList {
	if topN < len(c4s)/4 { // build heap only top 25% are need
		tmp := make(Item4sList, 0, len(c4s))

		for k, v := range c4s {
			tmp = append(tmp, Item4s{k, v})
		}

		heap.Init(&tmp)
		sorted := make(Item4sList, topN, topN)

		for i := 0; i < topN; i++ {
			sorted[i] = heap.Pop(&tmp).(Item4s)
		}

		return sorted
	}

	sorted := c4s.SortByValue()

	if topN > len(sorted) {
		topN = len(sorted)
	}

	return sorted[0:topN]
}

//////////////////////////////////////////////////////////////////////////////

// Generic Counter for interface
type Counter map[interface{}]int

// Make new Counter4s, length is the initialized size of map
func MakeCounter(length int) Counter {
	return make(Counter, length)
}

// Get ItemList of Counter without sorting
func (c Counter) GetItems() ItemList {
	seq := make(ItemList, len(c))
	idx := 0

	for k, v := range c {
		seq[idx] = Item{k, v}
		idx += 1
	}

	return seq
}

// Sort the Counter by counted values in descend order
func (c Counter) SortByValue() ItemList {
	seq := c.GetItems()
	sort.Sort(seq)
	return seq
}

// Sort the Counter by counted values in ascend order
func (c Counter) SortByValueAscend() ItemList {
	seq := c.GetItems()
	sort.Sort(sort.Reverse(seq))
	return seq
}

// Item of Counter
type Item struct {
	Key interface{}
	Val int
}

// For collect all items of Counter
type ItemList []Item

// Implement of sort interfaces for ItemList
// Note the default orderer is descend, means it's Great not Less
func (list ItemList) Less(i, j int) bool {
	return list[i].Val > list[j].Val
}

// Implement of sort interfaces for ItemList
func (list ItemList) Len() int {
	return len(list)
}

// Implement of sort interfaces for ItemList
func (list ItemList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}

// Heap interface implements for most common method
func (list *ItemList) Push(value interface{}) {
	*list = append(*list, value.(Item))
}

// Heap interface implements for most common method
func (list *ItemList) Pop() interface{} {
	old := *list
	num := len(old)
	ret := old[num-1]
	*list = old[0 : num-1]
	return ret
}

// An optimized most common method for large counter
func (c Counter) MostMommon(topN int) ItemList {
	if topN < len(c)/4 { // build heap only top 25% are need
		tmp := make(ItemList, 0, len(c))

		for k, v := range c {
			tmp = append(tmp, Item{k, v})
		}

		heap.Init(&tmp)
		sorted := make(ItemList, topN, topN)

		for i := 0; i < topN; i++ {
			sorted[i] = heap.Pop(&tmp).(Item)
		}

		return sorted
	}

	sorted := c.SortByValue()

	if topN > len(sorted) {
		topN = len(sorted)
	}

	return sorted[0:topN]
}

//////////////////////////////////////////////////////////////////////////////

// A string set(key unique) based on map
type Set4s map[string]struct{}

// Make new Set4s, length is the initialized size of map
func MakeSet4s(length int) Set4s {
	set := make(Set4s, length)
	return set
}

// Make new Set4s from seq
func MakeSet4sFrom(seq []string) Set4s {
	set := make(Set4s, len(seq)/3)

	for _, key := range seq {
		set[key] = struct{}{}
	}

	return set
}

// Clear(delete and new) the set
func (set *Set4s) Clear() {
	*set = make(Set4s)
}

// Add new key into set, return current length
func (set Set4s) Add(key string) int {
	set[key] = struct{}{}
	return len(set)
}

// Return items list of set without sorting
func (set Set4s) GetItems() []string {
	list := make([]string, 0, len(set))

	for key, _ := range set {
		list = append(list, key)
	}

	return list
}

// Remove a key from set, return key exists or not
func (set Set4s) Remove(key string) bool {
	if set.Contain(key) {
		delete(set, key)
		return true
	}
	return false
}

// Check the key is existed or not
func (set Set4s) Contain(key string) bool {
	_, exist := set[key]
	return exist
}

// Set algorithm: Union
func Set4sUnion(sets ...Set4s) Set4s {
	if len(sets) < 2 {
		return nil
	}

	union := make(Set4s)

	for _, set := range sets {
		for k, _ := range set {
			union.Add(k)
		}
	}

	return union
}

func set4s_intersection(s1, s2 Set4s) Set4s {
	intersect := make(Set4s)

	for k, _ := range s1 {
		if s2.Contain(k) {
			intersect.Add(k)
		}
	}

	return intersect
}

// Set algorithm: Intersection
func Set4sIntersection(sets ...Set4s) Set4s {
	if len(sets) < 2 {
		return nil
	}

	intersect := set4s_intersection(sets[0], sets[1])

	if len(intersect) == 0 || len(sets) == 2 {
		return intersect
	}

	for _, set := range sets[2:] {
		intersect = set4s_intersection(set, intersect)
		if len(intersect) == 0 {
			return intersect
		}
	}

	return intersect
}

// Set algorithm: Difference
func Set4sDifference(sets ...Set4s) Set4s {
	if len(sets) < 2 {
		return nil
	}

	diff := make(Set4s)
	union := Set4sUnion(sets...)
	intersect := Set4sIntersection(sets...)

	for k, _ := range union {
		if !intersect.Contain(k) {
			diff.Add(k)
		}
	}

	return diff
}

//////////////////////////////////////////////////////////////////////////////

// A generic set(key unique) based on map
type Set map[interface{}]struct{}

// Make new Set, length is the initialized size of map
func MakeSet(length int) Set {
	set := make(Set, length)
	return set
}

// Make new Set from seq
func MakeSetFrom(seq []interface{}) Set {
	set := make(Set, len(seq)/3)
	for _, key := range seq {
		set[key] = struct{}{}
	}
	return set
}

// Clear(delete and new) the set
func (set *Set) Clear() {
	*set = make(Set)
}

// Add new key into set, return current length
func (set Set) Add(key interface{}) int {
	set[key] = struct{}{}
	return len(set)
}

// Return items list of set without sorting
func (set Set) GetItems() []interface{} {
	list := make([]interface{}, 0, len(set))

	for key, _ := range set {
		list = append(list, key)
	}

	return list
}

// Remove a key from set, return key exists or not
func (set Set) Remove(key interface{}) bool {
	if set.Contain(key) {
		delete(set, key)
		return true
	}
	return false
}

// Check the key is existed or not
func (set Set) Contain(key interface{}) bool {
	_, exist := set[key]
	return exist
}

// Set algorithm: Union
func SetUnion(sets ...Set) Set {
	if len(sets) < 2 {
		return nil
	}

	union := make(Set)

	for _, set := range sets {
		for k, _ := range set {
			union.Add(k)
		}
	}

	return union
}

func set_intersection(s1, s2 Set) Set {
	intersect := make(Set)

	for k, _ := range s1 {
		if s2.Contain(k) {
			intersect.Add(k)
		}
	}

	return intersect
}

// Set algorithm: Intersection
func SetIntersection(sets ...Set) Set {
	if len(sets) < 2 {
		return nil
	}

	intersect := set_intersection(sets[0], sets[1])

	if len(intersect) == 0 || len(sets) == 2 {
		return intersect
	}

	for _, set := range sets[2:] {
		intersect = set_intersection(set, intersect)
		if len(intersect) == 0 {
			return intersect
		}
	}

	return intersect
}

// Set algorithm: Difference
func SetDifference(sets ...Set) Set {
	if len(sets) < 2 {
		return nil
	}

	diff := make(Set)
	union := SetUnion(sets...)
	intersect := SetIntersection(sets...)

	for k, _ := range union {
		if !intersect.Contain(k) {
			diff.Add(k)
		}
	}

	return diff
}

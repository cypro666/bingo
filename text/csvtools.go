// Helper for parse CSV file
package text

import (
	"bingo/fio"
	"encoding/csv"
	"errors"
	"os"
	"regexp"
	"strings"
)

const (
	CSV_DELIM    = `,`
	STDCSV_DELIM = `","`
	STDCSV_FIELD = `"(.+?)"`
)

// Parse 2-column csv file and return two map of col-1 => col-2 and col-2 => col-1
func Make2ColumnCsvAsMaps(csvname string, standard bool) (k2v, v2k map[string]string, err error) {
	fstat, err := os.Stat(csvname)

	if fstat.Size() > fio.GB*2 {
		err = errors.New("File is too large!")
		return
	}

	sre := STDCSV_FIELD + CSV_DELIM + STDCSV_FIELD
	search := regexp.MustCompile(sre).FindStringSubmatch

	lines, err := fio.ReadLines(csvname)
	if err != nil {
		return
	}

	k2v, v2k = map[string]string{}, map[string]string{}

	if standard {
		for _, line := range lines[:len(lines)-1] {
			seps := search(line)
			if len(seps) == 3 {
				k2v[seps[1]], v2k[seps[2]] = seps[2], seps[1]
			}
		}
	} else {
		for _, line := range lines[:len(lines)-1] {
			seps := strings.SplitN(line, CSV_DELIM, 2)
			if len(seps) == 2 {
				k2v[seps[0]], v2k[seps[1]] = seps[1], seps[0]
			}
		}
	}

	return k2v, v2k, nil
}

// Parse csv head line and return a map of field and it's index start from 0
func SplitCSVHeader(csvname string) (dict map[string]int, err error) {
	file, err := os.Open(csvname)
	if err != nil {
		return
	}
	defer file.Close()

	dict = make(map[string]int)
	reader := csv.NewReader(file)
	header, err := reader.Read()

	if err != nil {
		return
	}

	for i, value := range header {
		dict[value] = i
	}

	return
}

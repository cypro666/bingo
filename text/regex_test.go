package text

import (
	"testing"
)

func TestRegex(t *testing.T) {
	seps := RegexEnglish.FindAllString("cypro666@gmail.com", 100)
	if len(seps) != 3 {
		t.Fail()
	}
	ok := RegexURL.MatchString("https://github.com/cypro666/magic3/commit?id=123")
	if !ok {
		t.Fail()
	}
	if !RegexDate.MatchString("1995 12-03") {
		t.Fail()
	}
	if !RegexTime.MatchString("22:53:04") {
		t.Fail()
	}
	if !RegexTime.MatchString("05:55 PM") {
		t.Fail()
	}
	if !RegexEmail.MatchString("cypro666@gmail.com") {
		t.Fail()
	}
	if !RegexIPv4.MatchString("1.2.3.4") {
		t.Fail()
	}
	if !RegexIPv4.MatchString("0.0.0.255") {
		t.Fail()
	}
	if !RegexIPv4.MatchString("127.0.0.1") {
		t.Fail()
	}
	if !RegexIPv4.MatchString("133.144.55.90") {
		t.Fail()
	}
	if RegexIPv4.MatchString("333.2.3.4") {
		t.Fail()
	}
	if RegexIPv4.MatchString("-1.2.3.4") {
		t.Fail()
	}
	if RegexIPv4.MatchString("128.256.0") {
		t.Fail()
	}
	if RegexIPv4.MatchString("1.2.3.4.5") {
		t.Fail()
	}
}

// helper for write utf-8 file on windows
package text

func BOM() []byte {
	return []byte{0xEF, 0xBB, 0xBF}
}

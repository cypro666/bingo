package text

import (
	"bingo/fio"
	"testing"
)

func TestSqlMaker(t *testing.T) {
	var field = []string{"id", "name", "score"}
	var maker = NewSQLMaker("mytest", SQL_INSERT, field)

	if err := maker.OpenScript("../mytest.sql"); err != nil {
		t.Error(err)
	}

	var s string

	for i := 1; i <= 1000; i++ {
		s = maker.VMake(i, "joker", float32(len(s))/float32(i))
		maker.Script.WriteString(s)
	}

	maker.CloseScript()

	lines, _ := fio.ReadLines("../mytest.sql")

	if len(lines)-1 != 1002 {
		t.Fail()
	}
}

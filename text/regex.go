// A group of regex
package text

import (
	"regexp"
)

const (
	SRE_ENGLISH = "[A-Za-z]+"

	SRE_NONECHINESE = "[\u0000-\u4DFF]|[\u9FA5-\uFFFF]"

	SRE_NONE_CHINESE_ENGLISH = "[\u0000-\u002F]|[\u003A-\u0040]|[\u005B-\u0060]|[\u007B-\u4DFF]|[\u9FA5-\uFFFF]"

	SRE_IPV4 = "^([0-9]|[1-9][0-9]|1\\d\\d|2[0-4]\\d|25[0-5])\\.([0-9]|[1-9][0-9]|1\\d\\d|2[0-4]\\d|25[0-5])\\.([0-9]|[1-9][0-9]|1\\d\\d|2[0-4]\\d|25[0-5])\\.([0-9]|[1-9][0-9]|1\\d\\d|2[0-4]\\d|25[0-5])$"

	SRE_URL = "(https?|ftp)://[\\w\\-_]+(\\.[\\w\\-_]+).*?"

	SRE_URL_STRICT = "((https?|ftp):\\/\\/|\\/)[-A-Za-z0-9+&@#\\/%?=~_|!:,.;\\(\\)]+"

	SRE_DATE = "(\\d{2}|\\d{4})[ -/]([0-2][1-9])[ -/]([0-3][0-9])"

	SRE_TIME = "(\\d{1,2}):(\\d{2}):?(\\d{2})? ?(AM|PM)?"

	SRE_EMAIL = "([a-z0-9!#$%&'*+/=?^_`{|.}~-]+@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"

	SRE_STDCSV_FIELD = `"(.+?)"`
)

var RegexEnglish = regexp.MustCompile(SRE_ENGLISH)

var RegexNoneChinese = regexp.MustCompile(SRE_NONECHINESE)

var RegexNoneChineseEnglish = regexp.MustCompile(SRE_NONE_CHINESE_ENGLISH)

var RegexIPv4 = regexp.MustCompile(SRE_IPV4)

var RegexURL = regexp.MustCompile(SRE_URL)

var RegexURLStrict = regexp.MustCompile(SRE_URL_STRICT)

var RegexDate = regexp.MustCompile(SRE_DATE)

var RegexTime = regexp.MustCompile(SRE_TIME)

var RegexEmail = regexp.MustCompile(SRE_EMAIL)

var RegexStdCsvField = regexp.MustCompile(SRE_STDCSV_FIELD)

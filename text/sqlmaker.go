// Templater and Formater for making SQL scripts
package text

import (
	"fmt"
	"os"
	"strings"
)

var _sprintf = fmt.Sprintf

const (
	SQL_INSERT = "INSERT INTO `%s` (%s) VALUES(%s);\n"
	_START     = "START TRANSACTION;\n"
	_COMMIT    = "COMMIT;\n"
)

func SprintInsertFormat(table string, fieldname []string) string {
	return _sprintf(SQL_INSERT, table, "`"+strings.Join(fieldname, "`,`")+"`", "%s")
}

func SprintVarList(varlist ...interface{}) string {
	var s string

	for _, v := range varlist {
		s += _sprintf("\"%v\",", v)
	}

	return s[0 : len(s)-1]
}

type SQLMaker struct {
	qtemplte string
	format   string
	Script   *os.File
}

func NewSQLMaker(table_name, query_format string, field_names []string) *SQLMaker {
	sm := &SQLMaker{query_format, "", nil}
	sm.format = SprintInsertFormat(table_name, field_names)
	return sm
}

func MakeSQLMaker(table_name, query_format string, field_names []string) SQLMaker {
	return *NewSQLMaker(table_name, query_format, field_names)
}

func (this *SQLMaker) OpenScript(filename string) error {
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)

	if err != nil {
		return err
	}

	file.WriteString(_START)
	this.Script = file

	return nil
}

func (this *SQLMaker) CloseScript() {
	if this.Script != nil {
		this.Script.Sync()
		this.Script.WriteString(_COMMIT)
		this.Script.Sync()
		this.Script.Close()
	}
}

func (this *SQLMaker) VMake(values ...interface{}) string {
	return _sprintf(this.format, SprintVarList(values...))
}

func (this *SQLMaker) Make(values []interface{}) string {
	return _sprintf(this.format, SprintVarList(values...))
}

func (this *SQLMaker) GetFormat() string {
	return this.format
}

func (this *SQLMaker) GetQueryTemplate() string {
	return this.qtemplte
}

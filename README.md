# bingo  
## A group of utilities for golang  

### What's in it?  
  0  Some useful containers and algorithms like counter, set, skiplist and LINQ algorithms  
  1  An easy using and thread safe logger which may be the most lightest logger in golang  
  2  Utils for CSV, SQL files what makes you writing less code  
  3  A fast but light weight template engine, some string and bytes utils for text  
  4  A group of math functions to calculate statistics problems  
  5  Wrappers of json like simple-json and an easy using URL requests tool  
  6  Others: hashers' wrapper, filesystem API wrapper and some datetime utils like timedelta ...  


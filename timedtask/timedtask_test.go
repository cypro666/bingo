package timedtask

import (
	"fmt"
	"testing"
	"time"
)

var times_run int

func mytask(t time.Time) {
	fmt.Printf("time = %s\n", t.String())
	times_run++
}

func TestCron(t *testing.T) {
	fmt.Printf("TestCron\n")
	NewDailyTask(ANY, ANY, 10, mytask)
	RunTimedTasks()
	time.Sleep(61 * time.Second)
	if times_run != 2 {
		t.Errorf("%d\n", times_run)
	}
	fmt.Printf("PASS\n")
}

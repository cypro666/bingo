/*
  This package implements a simplistic Cron Task system that calls functions and
  closures on given days/times. If you need events that fire more often than
  once a second, use time.Ticker or time.After instead. The event must be able
  to accept a Time object (though it doesn't have to use it).
*/
package timedtask

import (
	"time"
)

type ttask struct {
	Month, Day, Weekday  int8
	Hour, Minute, Second int8
	Task                 func(time.Time)
}

const ANY = -1 // mod by MDR

var tasklist []ttask

// This function creates a new task that occurs at the given day and the given
// 24hour time. Any of the values may be -1 as an "any" match, so passing in
// a day of -1, the event occurs every day; passing in a second value of -1, the
// event will fire every second that the other parameters match.
func NewCronTask(month, day, weekday, hour, minute, second int8, task func(time.Time)) {
	t := ttask{month, day, weekday, hour, minute, second, task}
	tasklist = append(tasklist, t)
}

// This creates a task that fires monthly at a given time on a given day.
func NewMonthlyTask(day, hour, minute, second int8, task func(time.Time)) {
	NewCronTask(ANY, day, ANY, hour, minute, second, task)
}

// This creates a task that fires on the given day of the week and time.
func NewWeeklyTask(weekday, hour, minute, second int8, task func(time.Time)) {
	NewCronTask(ANY, ANY, weekday, hour, minute, second, task)
}

// This creates a task that fires daily at a specified time.
func NewDailyTask(hour, minute, second int8, task func(time.Time)) {
	NewCronTask(ANY, ANY, ANY, hour, minute, second, task)
}

func (tt ttask) Matches(t time.Time) (ok bool) {
	ok = (tt.Month == ANY || tt.Month == int8(t.Month())) &&
		(tt.Day == ANY || tt.Day == int8(t.Day())) &&
		(tt.Weekday == ANY || tt.Weekday == int8(t.Weekday())) &&
		(tt.Hour == ANY || tt.Hour == int8(t.Hour())) &&
		(tt.Minute == ANY || tt.Minute == int8(t.Minute())) &&
		(tt.Second == ANY || tt.Second == int8(t.Second()))
	return ok
}

func do_tasks() {
	for {
		now := time.Now()
		for _, j := range tasklist {
			// execute all our cron tasklist asynchronously
			if j.Matches(now) {
				go j.Task(now)
			}
		}
		time.Sleep(time.Millisecond * 500)
	}
}

func RunTimedTasks() {
	go do_tasks()
}

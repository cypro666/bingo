// Utils for reading file by line
package fio

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// Define default linebreak char and io buffer size
const (
	LINE_BREAK     = byte('\n')
	IO_BUFFER_SIZE = 8192
)

const (
	_ = 1 << (10 * iota)
	KB
	MB
	GB
	TB
	PB
)

// Call defer file.Close() after using this if err is nil
func MakeReader(filename string) (file *os.File, reader *bufio.Reader, err error) {
	file, err = os.Open(filename)
	if err != nil {
		return
	}

	reader = bufio.NewReaderSize(file, IO_BUFFER_SIZE)

	return
}

// Read file by line, need a callback accepted bytes of line
func ReadLineAsBytes(filename string, skiphead bool, callback func(bstr []byte)) (uint, error) {
	file, reader, err := MakeReader(filename)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	if skiphead {
		_, err := reader.ReadSlice(LINE_BREAK)
		if err != nil {
			return 0, err
		}
	}

	var nlines uint = 0

	for {
		bs, err := reader.ReadSlice(LINE_BREAK)
		switch err {
		case nil:
			nlines++
			if len(bs) > 0 {
				callback(bs[0 : len(bs)-1]) // strip
			} else {
				callback(bs)
			}
		case io.EOF:
			return nlines, nil
		default:
			return nlines, err
		}
	}

	return nlines, nil
}

// Read file by line, need a callback accepted string pointer of line
func ReadLineAsString(filename string, skiphead bool, callback func(str string)) (uint, error) {
	file, reader, err := MakeReader(filename)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	if skiphead {
		_, err := reader.ReadString(LINE_BREAK)
		if err != nil {
			return 0, err
		}
	}

	var nlines uint = 0

	for {
		str, err := reader.ReadString(LINE_BREAK)
		switch err {
		case nil:
			nlines++
			if len(str) > 0 {
				callback(str[0 : len(str)-1]) // strip
			} else {
				callback(str)
			}
		case io.EOF:
			return nlines, nil
		default:
			return nlines, err
		}
	}

	return nlines, nil
}

// Get first line of text file
func FirstLine(filename string) (line string, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if scanner.Scan() {
		line = scanner.Text()
	}

	return
}

// Get all lines in text file
func ReadLines(filename string) ([]string, error) {
	content, err := ioutil.ReadFile(filename)

	if err != nil {
		return nil, err
	}

	return strings.Split(string(content), string(LINE_BREAK)), nil
}

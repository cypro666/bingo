package fio

import (
	"bingo/system"
	"strings"
	"testing"
)

func TestFio(t *testing.T) {
	home, _ := system.HomeDir()
	name := home + "/.bash_history"
	if !system.IsExist(name) {
		t.Error(name)
		return
	}

	first, err := FirstLine(name)
	if err != nil {
		t.Error(err)
		return
	}

	lines, _ := ReadLines(name)
	if lines[0] != first {
		t.Error(lines[0])
		t.Error(first)
	}

	f, r, _ := MakeReader(name)
	defer f.Close()

	if line, _ := r.ReadString('\n'); strings.Trim(line, "\n") != first {
		t.Error(line)
	}

	var last1, last2 string
	var n1, n2 int

	ReadLineAsBytes(name, false, func(line []byte) {
		n1++
		last1 = string(line)
	})

	ReadLineAsString(name, false, func(line string) {
		n2++
		last2 = line
	})

	if n1 != n2 || last2 != last1 {
		t.Fail()
	}
}

// An easy using logger for Go, thread-safe with high performance
package easylog

import (
	"bytes"
	"fmt"
	"os"
	"testing"
)

func TestEasyLog(t *testing.T) {
	var log = Initialize(os.Stdout)

	DEBUG("debug", "foo")
	DEBUG("debug", "bar")
	INFO("info", "foo")
	INFO("info", "bar")
	WARN("warn", "foo")
	WARN("warn", "bar")
	ERROR("error", "foo")
	ERROR("error", "bar")

	ASSERT(0 == D)
	ASSERT(1 == I)
	ASSERT(2 == W)
	ASSERT(3 == E)
	ASSERT(4 == F)

	var dup = Initialize(os.Stdout)
	ASSERT(log == dup)

	b := bytes.Buffer{}
	fmt.Printf("%s OK\n", Join(&b, 1, "easylog_test.go", 32))
}

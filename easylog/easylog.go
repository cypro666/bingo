// Author: 	cypro666
// Date:	2016.05.01
// An easy using logger for Go, thread-safe with high performance
package easylog

import (
	"bytes"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Default global logger
// Do Not use this pointer in user code
var glog *EasyLog = nil

// Only 5 log levels
const (
	D int = iota // Debug
	I int = iota // INFO
	W int = iota // Warning
	E int = iota // Error
	F int = iota // Fatal or Final
)

// Internal lock for IO
var once_flag sync.Once

// For better performance in singel thread
type dummy_mutex struct{}

func (m *dummy_mutex) Lock() {
	// do nothing
}

func (m *dummy_mutex) Unlock() {
	// do nothing
}

// This simple logger has only one member io.Writer for writing data into
type EasyLog struct {
	mtx   sync.Locker
	file  *os.File
	buf   *bytes.Buffer
	level int
}

// Sync all data in buffer into opened log-file
func Flush() {
	glog.mtx.Lock()
	defer glog.mtx.Unlock()
	glog.file.Sync()
}

// Init global logger, if unsafe is true, dummy mutex will be used instead of sync.Mutex
func Initialize(file *os.File, unsafe ...bool) *EasyLog {
	if glog != nil {
		WARN("EasyLog has already initialized!!!")
	}

	once_flag.Do(func() {
		if len(unsafe) > 0 && unsafe[0] {
			glog = &EasyLog{&dummy_mutex{}, file, &bytes.Buffer{}, I}
		} else {
			glog = &EasyLog{&sync.Mutex{}, file, &bytes.Buffer{}, I}
		}
	})

	return glog
}

// Set log level
func SetLevel(level int) {
	glog.mtx.Lock()
	defer glog.mtx.Unlock()
	glog.level = level
}

// Join all log prefix
func Join(buf *bytes.Buffer, level int, name string, line int) string {
	buf.Reset()
	buf.WriteString(time.Now().String()[:26])

	switch level {
	case D:
		buf.WriteString(" DEBUG [")
	case I:
		buf.WriteString(" INFO  [")
	case W:
		buf.WriteString(" WARN  [")
	case E:
		buf.WriteString(" ERROR [")
	case F:
		buf.WriteString(" FATAL [")
	}

	buf.WriteString(strconv.Itoa(os.Getpid()))
	buf.WriteByte(' ')
	buf.WriteString(name)
	buf.WriteByte(':')
	buf.WriteString(strconv.Itoa(line))
	buf.WriteString("] ")

	return buf.String()
}

// Standard log style printer, it's multithread safe, output format like:
// 2016-04-12 18:01:29.244640 INFO  [6460 main.go:62] this is a info
func Log(file *os.File, degree int, level int, vargs ...interface{}) {
	glog.mtx.Lock()
	defer glog.mtx.Unlock()
	if level < glog.level {
		return
	}

	_, name, line, _ := runtime.Caller(degree)

	if pos := strings.LastIndex(name, "/"); pos != -1 {
		name = name[pos+1:]
	}

	fmt.Fprintf(file, Join(glog.buf, level, name, line))

	for _, v := range vargs {
		fmt.Fprintf(file, "%v ", v)
	}

	fmt.Fprintf(file, "\n")
}

// On level info
func INFO(v ...interface{}) {
	Log(glog.file, 2, I, v...)
}

// On level debug
func DEBUG(v ...interface{}) {
	Log(glog.file, 2, D, v...)
}

// On level warning
func WARN(v ...interface{}) {
	Log(glog.file, 2, W, v...)
}

// On level error
func ERROR(v ...interface{}) {
	Log(glog.file, 2, E, v...)
}

// On level fatal
func FATAL(v ...interface{}) {
	Log(glog.file, 2, F, v...)
}

// Safe assert(means exit when false)
func ASSERT(condition bool) {
	if false == condition {
		Log(os.Stderr, 2, E, "Assert Failed!")
		os.Exit(-1)
	}
}

// UnSafe assert(means no exit when false)
func ASSERT_NOEXIT(condition bool) {
	if false == condition {
		Log(os.Stderr, 2, E, "Assert Failed!")
	}
}

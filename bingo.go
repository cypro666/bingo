package bingo

import (
	_ "bingo/container"
	_ "bingo/easylog"
	_ "bingo/fio"
	_ "bingo/hasher"
	_ "bingo/linq"
	_ "bingo/requests"
	_ "bingo/simplejson"
	_ "bingo/statis"
	_ "bingo/system"
	_ "bingo/text"
	_ "bingo/timedtask"
	_ "bingo/utils"
)

const (
	VERSION  = "1.2 beta"
	AUTHOR   = "cypro666"
	REPOADDR = "github.com/cypro666/bingo"
)

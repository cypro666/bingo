package hasher

import (
	"testing"
)

func TestHash(t *testing.T) {
	s := "Version 1.0"
	b := []byte(s)
	h := NewHash()
	if !(uint64(2502632095216667848) == h.FNV64(b)) {
		t.Fail()
	}

	if !(uint32(411501462) == h.Adler32(b)) {
		t.Fail()
	}

	if !(uint64(1334755046848215436) == h.CRC64(b)) {
		t.Fail()
	}

	if !("3798985ee5e15c84c4263815d5a4d0b7" == h.MD5s(b)) {
		t.Fail()
	}
}

func TestPointerPool(t *testing.T) {
	s := "Version 1.0"
	pool := MakePointerPool()

	p, ok := pool.Get(s)

	if ok || p != nil {
		t.Fail()
	}

	p, ok = pool.MustGet(s)
	if !ok || p == nil {
		t.Fail()
	}

	if *p != s {
		t.Errorf("%v %v\n", *p, s)
		t.Fail()
	}
}

// Utils for hashing
package hasher

import (
	"crypto/md5"
	"fmt"
	"hash"
	"hash/adler32"
	"hash/crc32"
	"hash/crc64"
	"hash/fnv"
)

// Hasher struct, Not thread-safe!!!
type Hash struct {
	_fnv32 hash.Hash32
	_fnv64 hash.Hash64
	_crc32 hash.Hash32
	_crc64 hash.Hash64
	_adler hash.Hash32
	_md5   hash.Hash
}

// Create new hasher
func NewHash() *Hash {
	t32 := crc32.MakeTable(crc32.IEEE)
	t64 := crc64.MakeTable(crc64.ISO)

	return &Hash{
		fnv.New32(), fnv.New64(), crc32.New(t32), crc64.New(t64), adler32.New(), md5.New(),
	}
}

// Hash32 for data or uri string using FNV algorithm
func (this *Hash) FNV32(data []byte) uint32 {
	this._fnv32.Reset()
	this._fnv32.Write(data)
	return this._fnv32.Sum32()
}

// Hash64 for data or uri string using FNV algorithm
func (this *Hash) FNV64(data []byte) uint64 {
	this._fnv64.Reset()
	this._fnv64.Write(data)
	return this._fnv64.Sum64()
}

// CRC32 for data using ieee algorithm
func (this *Hash) CRC32(data []byte) uint32 {
	this._crc32.Reset()
	this._crc32.Write(data)
	return this._crc32.Sum32()
}

// CRC64 for data using iso algorithm
func (this *Hash) CRC64(data []byte) uint64 {
	this._crc64.Reset()
	this._crc64.Write(data)
	return this._crc64.Sum64()
}

// Adler32 for data using Adler algorithm
func (this *Hash) Adler32(data []byte) uint32 {
	this._adler.Reset()
	this._adler.Write(data)
	return this._adler.Sum32()
}

// MD5 for data using ssl algorithm
func (this *Hash) MD5(data []byte) []byte {
	this._md5.Reset()
	this._md5.Write(data)
	return this._md5.Sum(nil)
}

// MD5 for data using ssl algorithm, return readable string
func (this *Hash) MD5s(data []byte) string {
	return fmt.Sprintf("%x", this.MD5(data))
}

// Pool for string pointer
type PointerPool struct {
	_h2p  map[uint64]*string
	_hash hash.Hash64
}

func (sp *PointerPool) Get(s string) (*string, bool) {
	sp._hash.Reset()
	sp._hash.Write([]byte(s))
	k := sp._hash.Sum64()

	pstr, exist := sp._h2p[k]
	if exist {
		return pstr, *pstr == s
	}

	return nil, false
}

func (sp *PointerPool) MustGet(s string) (*string, bool) {
	sp._hash.Reset()
	sp._hash.Write([]byte(s))
	k := sp._hash.Sum64()

	pstr, exist := sp._h2p[k]

	if exist {
		return pstr, *pstr == s
	}

	sp._h2p[k] = &s

	return &s, true
}

func (sp *PointerPool) Remove(s string) {
	sp._hash.Reset()
	sp._hash.Write([]byte(s))
	k := sp._hash.Sum64()
	_, exist := sp._h2p[k]

	if exist {
		delete(sp._h2p, k)
	}
}

func MakePointerPool() PointerPool {
	return PointerPool{make(map[uint64]*string), fnv.New64()}
}

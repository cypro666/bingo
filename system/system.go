// A group of functions about file system and native api
package system

import (
	"os"
	"os/signal"
	"os/user"
	fp "path/filepath"
	"runtime"
	"strings"
	"unicode"
)

const (
	FilePerm = 0644
	DirPerm  = 0755
)

const (
	LINUX   = "linux"
	WINDOWS = "windows"
	DARWIN  = "darwin"
	FREEBSD = "freebsd"
	SOLARIS = "solaris"
	ANDROID = "android"
	UNKNOWN = "unknown"
)

func OS() string {
	return runtime.GOOS
}

func IsLinux() bool {
	return OS() == LINUX
}

func IsWindows() bool {
	return OS() == WINDOWS
}

func IsDarwin() bool {
	return OS() == DARWIN
}

func IsFreebsd() bool {
	return OS() == FREEBSD
}

func IsSolaris() bool {
	return OS() == SOLARIS
}

func IsAndroid() bool {
	return OS() == ANDROID
}

func EnvDef(env, def string) string {
	val := os.Getenv(env)
	if val == "" {
		return def
	}
	return val
}

// IsExist check whether or not file/dir exist
func IsExist(fname string) bool {
	_, err := os.Stat(fname)
	return !os.IsNotExist(err)
}

// get GOPATH from env
func GoPath() (dir string, ok bool) {
	dir, ok = os.LookupEnv("GOPATH")
	return
}

// More complete file stat struct
type FStat struct {
	Name   string
	Abs    string
	Base   string
	Parent string
	Seps   []string
	Stat   os.FileInfo
}

// Parse path and return new FStat
func NewFStat(path string) (fstat *FStat, err error) {
	stat, err := os.Stat(path)
	if err != nil {
		return
	}

	absp, _ := fp.Abs(path)

	fstat = &FStat{
		path,
		absp,
		fp.Base(path),
		fp.Dir(path),
		strings.SplitAfter(absp, string(os.PathSeparator)),
		stat,
	}

	return
}

// Walk in dir and return all file's FStat
func ListDir(name string) []FStat {
	var list = make([]FStat, 0, 10)

	fp.Walk(name, func(path string, _ os.FileInfo, err error) error {
		if err == nil {
			if fp, _ := NewFStat(path); fp != nil {
				list = append(list, *fp)
			}
		}
		return err
	})

	return list
}

// Walk in dir and call user function func(path string, isdir bool)
func ScanDir(name string, callback func(path string, isdir bool)) {
	fp.Walk(name, func(path string, info os.FileInfo, err error) error {
		callback(path, info.IsDir())
		return err
	})
}

// Get current user's home dir
func HomeDir() (path string, ok bool) {
	return os.LookupEnv("HOME")
}

// Home return current user's Home dir
func Home() string {
	u, _ := user.Current()
	return u.HomeDir
}

// ExpandHome expand ~ to user's home dir
func ExpandHome(path string) string {
	if len(path) == 0 || path[0] != '~' {
		return path
	}

	u, _ := user.Current()

	return u.HomeDir + path[1:]
}

// ExpandAbs expand path to absolute path
func ExpandAbs(path string) string {
	path, _ = fp.Abs(ExpandHome(path))
	return path
}

// ProgramDir return dir of program use os.Args[0]
func ProgramDir() (string, error) {
	return fp.Abs(fp.Dir(os.Args[0]))
}

// LastDir return last dir of path,
// if path is dir, return itself
// else return path's contain dir name
func LastDir(path string) (string, error) {
	absPath, err := fp.Abs(path)
	if err != nil {
		return "", err
	}

	info, err := os.Stat(absPath)
	if err != nil {
		return "", err
	}

	var dir string
	if info.IsDir() {
		_, dir = fp.Split(absPath)
	} else {
		dir = fp.Dir(absPath)
		_, dir = fp.Split(dir)
	}

	return dir, nil
}

// IsRelative check whether a path is relative
// In these condition: path is empty, start with '[.~][/\]', '/', "[a-z]:\"
func IsRelative(path string) bool {
	return strings.HasPrefix(path, ".") || strings.HasPrefix(path, "~") || !(strings.HasPrefix(path, "/") && !IsWinRoot(path))
}

// IsWinRoot check whether a path is windows absolute path with disk letter
func IsWinRoot(path string) bool {
	if path == "" {
		return false
	}
	return unicode.IsLetter(rune(path[0])) && strings.HasPrefix(path[1:], ":\\")
}

// IsRoot check wether or not path is root of filesystem
func IsRoot(path string) bool {
	l := len(path)

	if l == 0 {
		return false
	}

	switch OS() {
	case WINDOWS:
		return IsWinRoot(path)
	case LINUX, DARWIN, FREEBSD, SOLARIS, ANDROID:
		return l == 1 && path[0] == '/'
	default:
		return false
	}
}

func RemoveExt(path string) string {
	for i := len(path) - 1; i >= 0 && !os.IsPathSeparator(path[i]); i-- {
		if path[i] == '.' {
			return path[:i]
		}
	}
	return path
}

func ReplaceExt(path, ext string) string {
	if len(ext) > 0 && ext[0] != '.' {
		return RemoveExt(path) + "." + ext
	}
	return RemoveExt(path) + ext
}

// IsFile check whether or not file exist
func IsFile(fname string) bool {
	fi, err := os.Stat(fname)

	return err == nil && !fi.IsDir()
}

// IsDir check whether or not given name is a dir
func IsDir(fname string) bool {
	fi, err := os.Stat(fname)
	return err == nil && fi.IsDir()
}

// IsFileOrNotExist check whether given name is a file or not exist
func IsFileOrNotExist(fname string) bool {
	return !IsDir(fname)
}

// IsDirOrNotExist check whether given is a directory or not exist
func IsDirOrNotExist(dir string) bool {

	return !IsFile(dir)
}

// IsSymlink check whether or not given name is a symlink
func IsSymlink(fname string) bool {
	fi, err := os.Lstat(fname)

	return err == nil && (fi.Mode()&os.ModeSymlink == os.ModeSymlink)
}

// IsModifiedAfter check whether or not file is modified by the function
func IsModifiedAfter(fname string, fn func()) bool {
	fi1, err := os.Stat(fname)
	fn()
	fi2, _ := os.Stat(fname)
	return err == nil && !fi1.ModTime().Equal(fi2.ModTime())
}

// TruncSeek truncate file size to 0 and seek current positon to 0
func TruncSeek(fd *os.File) {
	if fd != nil {
		fd.Truncate(0)
		fd.Seek(0, os.SEEK_SET)
	}
}

// a simple signal notifier
func REGIST_SIGNAL_HANDLER(handler func(os.Signal), signals ...os.Signal) {
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, signals...)
		s := <-c
		handler(s)
	}()
}

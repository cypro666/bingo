package system

import (
	"os"
	fp "path/filepath"
	"testing"
)

func TestSystem(t *testing.T) {
	path, ok := os.LookupEnv("GOROOT")
	if !ok {
		t.Fatal("GOROOT")
	}
	if IsExist(path) != true {
		t.Fatal(path)
	}
	if IsDir(path) == false {
		t.Fatal(path)
	}

	fs, err := NewFStat(path)
	if err != nil {
		t.Fatal(err)
	}

	abs, _ := fp.Abs(path)
	if fs.Abs != abs {
		t.Fatal(path)
	}

	if fs.Base != fp.Base(path) {
		t.Fatal(path)
	}

	if fs.Parent != fp.Dir(path) {
		t.Fatal(path)
	}

	if len(fs.Seps) < 1 {
		t.Fatal(path)
	}

	ret := ListDir(path)
	if len(ret) == 0 {
		t.Error(path)
	}

	names := make([]string, 0)
	ScanDir(path, func(name string, isdir bool) {
		if (isdir && !IsDir(name)) || (!isdir && IsDir(name)) {
			t.Error(name)
		}
		if (isdir && IsFile(name)) || (!isdir && !IsFile(name)) {
			t.Error(name)
		}
		names = append(names, name)
	})

	if len(names) == 0 {
		t.Error(path)
	}

	if RemoveExt("bar.txt") != "bar" {
		t.Fail()
	}
	if ReplaceExt("foo.csv", "dat") != "foo.dat" {
		t.Fail()
	}

	if IsRelative("./foo/bar") == false {
		t.Fail()
	}

	if IsLinux() && !IsRoot("/") {
		t.Fail()
	}

}

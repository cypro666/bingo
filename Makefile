# Makefile of bingo library, require golang >= 1.6 
.PHONY: all

doc:
	godoc `pwd`

webdoc:
	godoc -http=:9090

format: 
	go fmt

test:
	go test "bingo/container"
	go test "bingo/easylog"
	go test "bingo/fio"
	go test "bingo/hasher"
	go test "bingo/linq"
	go test "bingo/requests"
	go test "bingo/simplejson"
	go test "bingo/statis"
	go test "bingo/system"
	go test "bingo/text"
	go test "bingo/timedtask"
	go test "bingo/statis"
	go test "bingo/utils"
	
check: format test

bingo: format
	go install "bingo/container"
	go install "bingo/easylog"
	go install "bingo/fio"
	go install "bingo/hasher"
	go install "bingo/linq"
	go install "bingo/requests"
	go install "bingo/simplejson"
	go install "bingo/statis"
	go install "bingo/system"
	go install "bingo/text"
	go install "bingo/timedtask"
	go install "bingo/statis"
	go install "bingo/utils"

all: bingo test


package utils

import (
	"fmt"
	"testing"
)

func TestUtils(t *testing.T) {
	a := []int{1, 2, 3}
	s := ToString(a)
	if s != fmt.Sprintf("%v", a) {
		t.Fail()
	}

	s = ToSyntaxString(a)
	if s != fmt.Sprintf("%#v", a) {
		t.Fail()
	}

	s = "A#B#C#D#E"

	r1, ok1 := NSplit(s, "#", 3)
	r2, ok2 := NSplitB([]byte(s), []byte("#"), 3)

	if len(r1) != 3 && ok1 {
		t.Fail()
	}
	if len(r2) != 3 && ok2 {
		t.Fail()
	}
	if IndexOf("B", r1) != 1 {
		t.Fail()
	}
	if IndexOfB([]byte("B"), r2) != 1 {
		t.Fail()
	}

	t.Logf("%v\n", IndexNth("B", r1, 1))

	s = TrimBlanks(" \vgood boy\t\r\n")
	if s != "good boy" {
		t.Fatal(s)
	}
}

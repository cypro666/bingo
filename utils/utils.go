// Utils
package utils

import (
	"bytes"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"strconv"
	"strings"
)

// Capacity in bytes
const (
	_ = 1 << (10 * iota)
	KB
	MB
	GB
	TB
	PB
)

// Return stack list for printing
func StackInfo(full bool) string {
	var buf [KB * 4]byte
	n := runtime.Stack(buf[:], full)
	return string(buf[:n])
}

//Print stack info
func PrintStack() {
	fmt.Fprintf(os.Stderr, "%s\n", StackInfo(false))
}

// Call this only in main once
func RunTimeInit(max_num_threads int) {
	if max_num_threads == 0 {
		if runtime.NumCPU() == 1 {
			runtime.GOMAXPROCS(1)
		} else {
			runtime.GOMAXPROCS(runtime.NumCPU()/2 + 1)
		}
	} else {
		runtime.GOMAXPROCS(max_num_threads)
	}
}

// String to int64 ignore error
func Atoi64(s string) int64 {
	n, _ := strconv.ParseInt(s, 10, 64)
	return n
}

// Int64 to string
func Itoa64(i int64) string {
	return strconv.FormatInt(i, 10)
}

// To string for everything
func ToString(val interface{}) string {
	return fmt.Sprintf("%v", val)
}

// To syntax string for everything
func ToSyntaxString(val interface{}) string {
	return fmt.Sprintf("%#v", val)
}

// Split string and check length of results
func NSplit(str, delim string, expect int) (seq []string, ok bool) {
	seq = strings.SplitN(str, delim, expect)
	ok = len(seq) == expect
	return
}

// Split bytes and check length of results
func NSplitB(bs, delim []byte, expect int) (seq [][]byte, ok bool) {
	seq = bytes.SplitN(bs, delim, expect)
	ok = len(seq) == expect
	return
}

// Get index of value in the string
func IndexOf(value string, seq []string) int {
	for idx, item := range seq {
		if item == value {
			return idx
		}
	}
	return -1
}

// Get index of value in the bytes slice
func IndexOfB(value []byte, seq [][]byte) int {
	for idx, item := range seq {
		if string(item) == string(value) {
			return idx
		}
	}
	return -1
}

// Get index of N-th value in the string
func IndexNth(value string, seq []string, nth int) int {
	for idx, item := range seq {
		if item == value {
			nth--
			if nth == 0 {
				return idx
			}
		}
	}
	return -1
}

// Get index of N-th value in the bytes slice
func IndexNthB(value []byte, seq [][]byte, nth int) int {
	for idx, item := range seq {
		if string(item) == string(value) {
			nth--
			if nth == 0 {
				return idx
			}
		}
	}
	return -1
}

// Trim all blank chars of string
func TrimBlanks(s string) string {
	return strings.Trim(s, " \t\v\r\n\\0")
}

// Trim all blank chars of string only on left
func TrimLeftBlanks(s string) string {
	return strings.TrimLeft(s, " \t\v\r\n\\0")
}

// Trim all blank chars of string only on right
func TrimRightBlanks(s string) string {
	return strings.TrimRight(s, " \t\v\r\n\\0")
}

// Filter for each string in strings slice
func StringSliceFilter(slice []string, filter func(string) bool) []string {
	ret := make([]string, 0, len(slice)/2)
	for _, s := range slice {
		if filter(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

// Filter for each []byte in [][]byte slice
func BytesSliceFilter(slice [][]byte, filter func([]byte) bool) [][]byte {
	ret := make([][]byte, 0, len(slice)/2)
	for _, s := range slice {
		if filter(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

// Filter for interface{} slice
func Filter(slice []interface{}, filter func(interface{}) bool) []interface{} {
	ret := make([]interface{}, 0, len(slice)/2)
	for _, s := range slice {
		if filter(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func fmtatom(v reflect.Value) string {
	switch v.Kind() {
	case reflect.Invalid:
		return "Invalid"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return strconv.FormatInt(v.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return strconv.FormatUint(v.Uint(), 10)
	case reflect.Uintptr:
		return "0x" + strconv.FormatUint(v.Uint(), 16)
	case reflect.Bool:
		return strconv.FormatBool(v.Bool())
	case reflect.Float32, reflect.Float64:
		return strconv.FormatFloat(v.Float(), 'e', -1, 64)
	case reflect.String:
		return strconv.Quote(v.String())
	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		return "0x" + strconv.FormatUint(uint64(v.Pointer()), 16)
	default:
		return v.Type().String() + " value"
	}
}

func pprint(path string, v reflect.Value) {
	switch v.Kind() {
	case reflect.Invalid:
		fmt.Printf("%s = invalid\n", path)
	case reflect.Slice, reflect.Array:
		for i := 0; i < v.Len(); i++ {
			pprint(fmt.Sprintf("%s[%d]", path, i), v.Index(i))
		}
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			pprint(fmt.Sprintf("%s.%s", path, v.Type().Field(i).Name), v.Field(i))
		}
	case reflect.Map:
		for _, key := range v.MapKeys() {
			pprint(fmt.Sprintf("%s[%s]", path, fmtatom(key)), v.MapIndex(key))
		}
	case reflect.Ptr:
		if v.IsNil() {
			fmt.Printf("%s = nil\n", path)
		} else {
			pprint(fmt.Sprintf("(*%s)", path), v.Elem())
		}
	case reflect.Interface:
		if v.IsNil() {
			fmt.Printf("%s = nil\n")
		} else {
			fmt.Printf("%s.type = %s\n", path, v.Elem().Type())
			pprint(path+".value", v.Elem())
		}
	default:
		fmt.Printf("%s = %s\n", path, fmtatom(v))
	}
}

// Deep and pretty printer for composite structs in debugging
func PrettyPrint(name string, v interface{}) {
	pprint(name, reflect.ValueOf(v))
}

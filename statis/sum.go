package statis

import (
	"errors"
	"math"
)

// Sum adds all the numbers of a slice together
func Sum(input Float64Data) (sum float64, err error) {

	if input.Len() == 0 {
		return math.NaN(), EmptyInput
	}

	// Add em up
	for _, n := range input {
		sum += n
	}

	return sum, nil
}

// Sum adds all the numbers of a slice together
func SumWeight(input Float64Data, weight Float64Data) (sum float64, err error) {

	if input.Len() == 0 {
		return math.NaN(), EmptyInput
	}

	if len(input) != len(weight) {
		return 0, errors.New("Length of input and weight is Not equal")
	}

	// Add em up
	for i, n := range input {
		sum += n * weight[i]
	}

	return sum, nil
}

package statis

import (
	"math"
	"math/rand"
	"reflect"
	"runtime"
	"sort"
	"testing"
	"time"
)

func TestCorrelation(t *testing.T) {
	s1 := []float64{1, 2, 3, 4, 5}
	s2 := []float64{10, -51.2, 8}
	s3 := []float64{1, 2, 3, 5, 6}
	s4 := []float64{}
	s5 := []float64{0, 0, 0}

	a, err := Correlation(s5, s5)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}
	if a != 0 {
		t.Errorf("Should have returned 0")
	}

	_, err = Correlation(s1, s2)
	if err == nil {
		t.Errorf("Mismatched slice lengths should have returned an error")
	}

	a, err = Correlation(s1, s3)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}

	if a != 0.9912407071619302 {
		t.Errorf("Correlation %v != %v", a, 0.9912407071619302)
	}

	_, err = Correlation(s1, s4)
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}

	a, err = Pearson(s1, s3)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}

	if a != 0.9912407071619302 {
		t.Errorf("Correlation %v != %v", a, 0.9912407071619302)
	}

}

var data1 = Float64Data{-10, -10.001, 5, 1.1, 2, 3, 4.20, 5}
var data2 = Float64Data{-9, -9.001, 4, .1, 1, 2, 3.20, 5}

func getFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func checkResult(result float64, err error, name string, f float64, t *testing.T) {
	if err != nil {
		t.Errorf("%s returned an error", name)
	}
	if result != f {
		t.Errorf("%s() => %v != %v", name, result, f)
	}
}

func TestInterfaceMethods(t *testing.T) {
	// Test Get
	a := data1.Get(1)
	if a != -10.001 {
		t.Errorf("Get(2) => %.1f != %.1f", a, -10.001)
	}

	// Test Len
	l := data1.Len()
	if l != 8 {
		t.Errorf("Len() => %v != %v", l, 8)
	}

	// Test Less
	b := data1.Less(0, 5)
	if b != true {
		t.Errorf("Less() => %v != %v", b, true)
	}

	// Test Swap
	data1.Swap(0, 2)
	if data1.Get(0) != 5 {
		t.Errorf("Len() => %v != %v", l, 8)
	}
}

func TestHelperMethods(t *testing.T) {

	// Test Min
	m, _ := data1.Min()
	if m != -10.001 {
		t.Errorf("Min() => %v != %v", m, -10.001)
	}

	// Test Max
	m, _ = data1.Max()
	if m != 5 {
		t.Errorf("Max() => %v != %v", m, 5)
	}

	// Test Sum
	m, _ = data1.Sum()
	if m != 0.2990000000000004 {
		t.Errorf("Sum() => %v != %v", m, 0.2990000000000004)
	}

	// Test Mean
	m, _ = data1.Mean()
	if m != 0.03737500000000005 {
		t.Errorf("Mean() => %v != %v", m, 0.03737500000000005)
	}

	// Test GeometricMean
	m, _ = data1.GeometricMean()
	if m != 4.028070682618703 {
		t.Errorf("GeometricMean() => %v != %v", m, 4.028070682618703)
	}

	// Test HarmonicMean
	m, _ = data1.HarmonicMean()
	if !math.IsNaN(m) {
		t.Errorf("HarmonicMean() => %v != %v", m, math.NaN())
	}

	// Test Median
	m, _ = data1.Median()
	if m != 2.5 {
		t.Errorf("Median() => %v != %v", m, 2.5)
	}

	// Test Mode
	mo, _ := data1.Mode()
	if !reflect.DeepEqual(mo, []float64{5.0}) {
		t.Errorf("Mode() => %.1f != %.1f", mo, []float64{5.0})
	}
}

func assertFloat64(fn func() (float64, error), f float64, t *testing.T) {
	res, err := fn()
	checkResult(res, err, getFunctionName(fn), f, t)
}

func TestMedianAbsoluteDeviationMethods(t *testing.T) {
	assertFloat64(data1.MedianAbsoluteDeviation, 2.1, t)
	assertFloat64(data1.MedianAbsoluteDeviationPopulation, 2.1, t)
}

func TestStandardDeviationMethods(t *testing.T) {
	assertFloat64(data1.StandardDeviation, 5.935684731720091, t)
	assertFloat64(data1.StandardDeviationPopulation, 5.935684731720091, t)
	assertFloat64(data1.StandardDeviationSample, 6.345513892000508, t)
}

func TestVarianceMethods(t *testing.T) {
	assertFloat64(data1.Variance, 35.232353234375005, t)
	assertFloat64(data1.PopulationVariance, 35.232353234375005, t)
	assertFloat64(data1.SampleVariance, 40.26554655357143, t)

}

func assertPercentiles(fn func(i float64) (float64, error), i float64, f float64, t *testing.T) {
	res, err := fn(i)
	checkResult(res, err, getFunctionName(fn), f, t)
}

func TestPercentileMethods(t *testing.T) {
	assertPercentiles(data1.Percentile, 75, 4.6, t)
	assertPercentiles(data1.PercentileNearestRank, 75, 4.2, t)

}

func assertOtherDataMethods(fn func(d Float64Data) (float64, error), d Float64Data, f float64, t *testing.T) {
	res, err := fn(d)
	checkResult(res, err, getFunctionName(fn), f, t)
}

func TestOtherDataMethods(t *testing.T) {
	assertOtherDataMethods(data1.Correlation, data2, 0.20875473597605448, t)
	assertOtherDataMethods(data1.Pearson, data2, 0.20875473597605448, t)
	assertOtherDataMethods(data1.InterQuartileRange, data2, 8.05, t)
	assertOtherDataMethods(data1.Midhinge, data2, -0.42500000000000004, t)
	assertOtherDataMethods(data1.Trimean, data2, 0.5375, t)
	assertOtherDataMethods(data1.Covariance, data2, 7.3814215535714265, t)
	assertOtherDataMethods(data1.CovariancePopulation, data2, 6.458743859374998, t)
}

func TestSampleMethod(t *testing.T) {
	// Test Sample method
	_, err := data1.Sample(5, true)
	if err != nil {
		t.Errorf("%s returned an error", getFunctionName(data1.Sample))
	}
}

func TestQuartileMethods(t *testing.T) {
	// Test QuartileOutliers method
	_, err := data1.QuartileOutliers()
	if err != nil {
		t.Errorf("%s returned an error", getFunctionName(data1.QuartileOutliers))
	}

	// Test Quartile method
	_, err = data1.Quartile(data2)
	if err != nil {
		t.Errorf("%s returned an error", getFunctionName(data1.Quartile))
	}
}

// Here we show the regular way of doing it
// with a plain old slice of float64s
func BenchmarkRegularAPI(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data := []float64{-10, -7, -3.11, 5, 1.1, 2, 3, 4.20, 5, 18}
		Min(data)
		Max(data)
		Sum(data)
		Mean(data)
		Median(data)
		Mode(data)
	}
}

// Here's where things get interesting
// and we start to use the included
// Float64Data type and methods
func BenchmarkMethodsAPI(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data := Float64Data{-10, -7, -3.11, 5, 1.1, 2, 3, 4.20, 5, 18}
		data.Min()
		data.Max()
		data.Sum()
		data.Mean()
		data.Median()
		data.Mode()
	}
}

func TestMedianAbsoluteDeviation(t *testing.T) {
	_, err := MedianAbsoluteDeviation([]float64{1, 2, 3})
	if err != nil {
		t.Errorf("Returned an error")
	}
}

func TestMedianAbsoluteDeviationPopulation(t *testing.T) {
	s, _ := MedianAbsoluteDeviation([]float64{1, 2, 3})
	m, err := Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 1.00 {
		t.Errorf("%.10f != %.10f", m, 1.00)
	}

	s, _ = MedianAbsoluteDeviation([]float64{-2, 0, 4, 5, 7})
	m, err = Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 3.00 {
		t.Errorf("%.10f != %.10f", m, 3.00)
	}

	m, _ = MedianAbsoluteDeviation([]float64{})
	if !math.IsNaN(m) {
		t.Errorf("%.1f != %.1f", m, math.NaN())
	}
}

func TestStandardDeviation(t *testing.T) {
	_, err := StandardDeviation([]float64{1, 2, 3})
	if err != nil {
		t.Errorf("Returned an error")
	}
}

func TestStandardDeviationPopulation(t *testing.T) {
	s, _ := StandardDeviationPopulation([]float64{1, 2, 3})
	m, err := Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 0.82 {
		t.Errorf("%.10f != %.10f", m, 0.82)
	}
	s, _ = StandardDeviationPopulation([]float64{-1, -2, -3.3})
	m, err = Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 0.94 {
		t.Errorf("%.10f != %.10f", m, 0.94)
	}

	m, _ = StandardDeviationPopulation([]float64{})
	if !math.IsNaN(m) {
		t.Errorf("%.1f != %.1f", m, math.NaN())
	}
}

func TestStandardDeviationSample(t *testing.T) {
	s, _ := StandardDeviationSample([]float64{1, 2, 3})
	m, err := Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 1.0 {
		t.Errorf("%.10f != %.10f", m, 1.0)
	}
	s, _ = StandardDeviationSample([]float64{-1, -2, -3.3})
	m, err = Round(s, 2)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if m != 1.15 {
		t.Errorf("%.10f != %.10f", m, 1.15)
	}

	m, _ = StandardDeviationSample([]float64{})
	if !math.IsNaN(m) {
		t.Errorf("%.1f != %.1f", m, math.NaN())
	}
}

func TestError(t *testing.T) {
	err := statsErr{"test error"}
	if err.Error() != "test error" {
		t.Errorf("Error method message didn't match")
	}
}

// Create working sample data to test if the legacy
// functions cause a runtime crash or return an error
func TestLegacy(t *testing.T) {

	// Slice of data
	s := []float64{-10, -10.001, 5, 1.1, 2, 3, 4.20, 5}

	// Slice of coordinates
	d := []Coordinate{
		{1, 2.3},
		{2, 3.3},
		{3, 3.7},
		{4, 4.3},
		{5, 5.3},
	}

	// VarP rename compatibility
	_, err := VarP(s)
	if err != nil {
		t.Errorf("VarP not successfully returning PopulationVariance.")
	}

	// VarS rename compatibility
	_, err = VarS(s)
	if err != nil {
		t.Errorf("VarS not successfully returning SampleVariance.")
	}

	// StdDevP rename compatibility
	_, err = StdDevP(s)
	if err != nil {
		t.Errorf("StdDevP not successfully returning StandardDeviationPopulation.")
	}

	// StdDevS rename compatibility
	_, err = StdDevS(s)
	if err != nil {
		t.Errorf("StdDevS not successfully returning StandardDeviationSample.")
	}

	// LinReg rename compatibility
	_, err = LinReg(d)
	if err != nil {
		t.Errorf("LinReg not successfully returning LinearRegression.")
	}

	// ExpReg rename compatibility
	_, err = ExpReg(d)
	if err != nil {
		t.Errorf("ExpReg not successfully returning ExponentialRegression.")
	}

	// LogReg rename compatibility
	_, err = LogReg(d)
	if err != nil {
		t.Errorf("LogReg not successfully returning LogarithmicRegression.")
	}
}

var allTestData = []struct {
	actual   interface{}
	expected Float64Data
}{
	{
		[]interface{}{1.0, "2", 3.0, 4, "4.0", 5, time.Duration(6), time.Duration(-7)},
		Float64Data{1.0, 2.0, 3.0, 4.0, 4.0, 5.0, 6.0, -7.0},
	},
	{
		[]interface{}{"-345", "223", "-654.4", "194", "898.3"},
		Float64Data{-345.0, 223.0, -654.4, 194.0, 898.3},
	},
	{
		[]interface{}{7862, 4234, 9872.1, 8794},
		Float64Data{7862.0, 4234.0, 9872.1, 8794.0},
	},
	{
		[]interface{}{true, false, true, false, false},
		Float64Data{1.0, 0.0, 1.0, 0.0, 0.0},
	},
	{
		[]interface{}{14.3, 26, 17.7, "shoe"},
		Float64Data{14.3, 26.0, 17.7},
	},
	{
		[]uint{34, 12, 65, 230, 30},
		Float64Data{34.0, 12.0, 65.0, 230.0, 30.0},
	},
	{
		[]bool{true, false, true, true, false},
		Float64Data{1.0, 0.0, 1.0, 1.0, 0.0},
	},
	{
		[]float64{10230.9823, 93432.9384, 23443.945, 12374.945},
		Float64Data{10230.9823, 93432.9384, 23443.945, 12374.945},
	},
	{
		[]int{-843, 923, -398, 1000},
		Float64Data{-843.0, 923.0, -398.0, 1000.0},
	},
	{
		[]time.Duration{-843, 923, -398, 1000},
		Float64Data{-843.0, 923.0, -398.0, 1000.0},
	},
	{
		[]string{"-843.2", "923", "hello", "-398", "1000.5"},
		Float64Data{-843.2, 923.0, -398.0, 1000.5},
	},
	{
		map[int]int{0: 456, 1: 758, 2: -9874, 3: -1981},
		Float64Data{456.0, 758.0, -9874.0, -1981.0},
	},
	{
		map[int]float64{0: 68.6, 1: 72.1, 2: -33.3, 3: -99.2},
		Float64Data{68.6, 72.1, -33.3, -99.2},
	},
	{
		map[int]uint{0: 4567, 1: 7580, 2: 98742, 3: 19817},
		Float64Data{4567.0, 7580.0, 98742.0, 19817.0},
	},
	{
		map[int]string{0: "456", 1: "758", 2: "-9874", 3: "-1981", 4: "68.6", 5: "72.1", 6: "-33.3", 7: "-99.2"},
		Float64Data{456.0, 758.0, -9874.0, -1981.0, 68.6, 72.1, -33.3, -99.2},
	},
	{
		map[int]bool{0: true, 1: true, 2: false, 3: true, 4: false},
		Float64Data{1.0, 1.0, 0.0, 1.0, 0.0},
	},
}

func equal(actual, expected Float64Data) bool {
	if len(actual) != len(expected) {
		return false
	}

	for k, actualVal := range actual {
		if actualVal != expected[k] {
			return false
		}
	}

	return true
}

func TestLoadRawData(t *testing.T) {
	for _, data := range allTestData {
		actual := LoadRawData(data.actual)
		if !equal(actual, data.expected) {
			t.Fatalf("Transform(%v). Expected [%v], Actual [%v]", data.actual, data.expected, actual)
		}
	}
}

func TestMax(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{[]float64{1, 2, 3, 4, 5}, 5.0},
		{[]float64{10.5, 3, 5, 7, 9}, 10.5},
		{[]float64{-20, -1, -5.5}, -1.0},
		{[]float64{-1.0}, -1.0},
	} {
		got, err := Max(c.in)
		if err != nil {
			t.Errorf("Returned an error")
		}
		if got != c.out {
			t.Errorf("Max(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Max([]float64{})
	if err == nil {
		t.Errorf("Empty slice didn't return an error")
	}
}

func BenchmarkMaxSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Max(makeFloatSlice(5))
	}
}

func BenchmarkMaxLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Max(lf)
	}
}

func TestMean(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{[]float64{1, 2, 3, 4, 5}, 3.0},
		{[]float64{1, 2, 3, 4, 5, 6}, 3.5},
		{[]float64{1}, 1.0},
	} {
		got, _ := Mean(c.in)
		if got != c.out {
			t.Errorf("Mean(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Mean([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func BenchmarkMeanSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Mean(makeFloatSlice(5))
	}
}

func BenchmarkMeanLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Mean(lf)
	}
}

func TestGeometricMean(t *testing.T) {
	s1 := []float64{2, 18}
	s2 := []float64{10, 51.2, 8}
	s3 := []float64{1, 3, 9, 27, 81}

	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{s1, 6},
		{s2, 16},
		{s3, 9},
	} {
		gm, err := GeometricMean(c.in)
		if err != nil {
			t.Errorf("Should not have returned an error")
		}

		gm, _ = Round(gm, 0)
		if gm != c.out {
			t.Errorf("Geometric Mean %v != %v", gm, c.out)
		}
	}

	_, err := GeometricMean([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestHarmonicMean(t *testing.T) {
	s1 := []float64{1, 2, 3, 4, 5}
	s2 := []float64{10, -51.2, 8}
	s3 := []float64{1, 0, 9, 27, 81}

	hm, err := HarmonicMean(s1)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}

	hm, _ = Round(hm, 2)
	if hm != 2.19 {
		t.Errorf("Geometric Mean %v != %v", hm, 2.19)
	}

	hm, err = HarmonicMean(s2)
	if err == nil {
		t.Errorf("Should have returned a negative number error")
	}

	hm, err = HarmonicMean(s3)
	if err == nil {
		t.Errorf("Should have returned a zero number error")
	}

	_, err = HarmonicMean([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestMedian(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{[]float64{5, 3, 4, 2, 1}, 3.0},
		{[]float64{6, 3, 2, 4, 5, 1}, 3.5},
		{[]float64{1}, 1.0},
	} {
		got, _ := Median(c.in)
		if got != c.out {
			t.Errorf("Median(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Median([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func BenchmarkMedianSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Median(makeFloatSlice(5))
	}
}

func BenchmarkMedianLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Median(lf)
	}
}

func TestMedianSortSideEffects(t *testing.T) {
	s := []float64{0.1, 0.3, 0.2, 0.4, 0.5}
	a := []float64{0.1, 0.3, 0.2, 0.4, 0.5}
	Median(s)
	if !reflect.DeepEqual(s, a) {
		t.Errorf("%.1f != %.1f", s, a)
	}
}

func TestMin(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{[]float64{1.1, 2, 3, 4, 5}, 1.1},
		{[]float64{10.534, 3, 5, 7, 9}, 3.0},
		{[]float64{-5, 1, 5}, -5.0},
		{[]float64{5}, 5},
	} {
		got, err := Min(c.in)
		if err != nil {
			t.Errorf("Returned an error")
		}
		if got != c.out {
			t.Errorf("Min(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Min([]float64{})
	if err == nil {
		t.Errorf("Empty slice didn't return an error")
	}
}

func BenchmarkMinSmallFloatSlice(b *testing.B) {
	testData := makeFloatSlice(5)
	for i := 0; i < b.N; i++ {
		Min(testData)
	}
}

func BenchmarkMinSmallRandFloatSlice(b *testing.B) {
	testData := makeRandFloatSlice(5)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Min(testData)
	}
}

func BenchmarkMinLargeFloatSlice(b *testing.B) {
	testData := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Min(testData)
	}
}

func BenchmarkMinLargeRandFloatSlice(b *testing.B) {
	testData := makeRandFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Min(testData)
	}
}

func TestMode(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out []float64
	}{
		{[]float64{5, 3, 4, 2, 1}, []float64{}},
		{[]float64{5, 5, 3, 4, 2, 1}, []float64{5}},
		{[]float64{5, 5, 3, 3, 4, 2, 1}, []float64{3, 5}},
		{[]float64{1}, []float64{1}},
		{[]float64{-50, -46.325, -46.325, -.87, 1, 2.1122, 3.20, 5, 15, 15, 15.0001}, []float64{-46.325, 15}},
		{[]float64{1, 2, 3, 4, 4, 4, 4, 4, 5, 3, 6, 7, 5, 0, 8, 8, 7, 6, 9, 9}, []float64{4}},
		{[]float64{76, 76, 110, 76, 76, 76, 76, 119, 76, 76, 76, 76, 31, 31, 31, 31, 83, 83, 83, 78, 78, 78, 78, 78, 78, 78, 78}, []float64{76}},
	} {
		got, err := Mode(c.in)
		if err != nil {
			t.Errorf("Returned an error")
		}
		sort.Float64s(got)
		if !reflect.DeepEqual(c.out, got) {
			t.Errorf("Mode(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Mode([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func BenchmarkModeSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Mode(makeFloatSlice(5))
	}
}

func BenchmarkModeSmallRandFloatSlice(b *testing.B) {
	lf := makeRandFloatSlice(5)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Mode(lf)
	}
}

func BenchmarkModeLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Mode(lf)
	}
}

func BenchmarkModeLargeRandFloatSlice(b *testing.B) {
	lf := makeRandFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Mode(lf)
	}
}

func TestQuartileOutliers(t *testing.T) {
	s1 := []float64{-1000, 1, 3, 4, 4, 6, 6, 6, 6, 7, 8, 15, 18, 100}
	o, _ := QuartileOutliers(s1)

	if o.Mild[0] != 15 {
		t.Errorf("First Mild Outlier %v != 15", o.Mild[0])
	}

	if o.Mild[1] != 18 {
		t.Errorf("Second Mild Outlier %v != 18", o.Mild[1])
	}

	if o.Extreme[0] != -1000 {
		t.Errorf("First Extreme Outlier %v != -1000", o.Extreme[0])
	}

	if o.Extreme[1] != 100 {
		t.Errorf("Second Extreme Outlier %v != 100", o.Extreme[1])
	}

	_, err := QuartileOutliers([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestPercentile(t *testing.T) {
	m, _ := Percentile([]float64{43, 54, 56, 61, 62, 66}, 90)
	if m != 62.0 {
		t.Errorf("%.1f != %.1f", m, 62.0)
	}
	m, _ = Percentile([]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 50)
	if m != 5.5 {
		t.Errorf("%.1f != %.1f", m, 5.5)
	}
	m, _ = Percentile([]float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 99.9)
	if m != 10.0 {
		t.Errorf("%.1f != %.1f", m, 10.0)
	}
	_, err := Percentile([]float64{}, 99.9)
	if err == nil {
		t.Errorf("Empty slice didn't return an error")
	}
	_, err = Percentile([]float64{1, 2, 3, 4, 5}, 0)
	if err == nil {
		t.Errorf("Zero percent didn't return an error")
	}
}

func TestPercentileSortSideEffects(t *testing.T) {
	s := []float64{43, 54, 56, 44, 62, 66}
	a := []float64{43, 54, 56, 44, 62, 66}
	Percentile(s, 90)
	if !reflect.DeepEqual(s, a) {
		t.Errorf("%.1f != %.1f", s, a)
	}
}

func BenchmarkPercentileSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Percentile(makeFloatSlice(5), 50)
	}
}

func BenchmarkPercentileLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Percentile(lf, 50)
	}
}

func TestPercentileNearestRank(t *testing.T) {
	f1 := []float64{35, 20, 15, 40, 50}
	f2 := []float64{20, 6, 7, 8, 8, 10, 13, 15, 16, 3}
	f3 := makeFloatSlice(101)

	for _, c := range []struct {
		sample  []float64
		percent float64
		result  float64
	}{
		{f1, 30, 20},
		{f1, 40, 20},
		{f1, 50, 35},
		{f1, 75, 40},
		{f1, 95, 50},
		{f1, 99, 50},
		{f1, 99.9, 50},
		{f1, 100, 50},
		{f2, 25, 7},
		{f2, 50, 8},
		{f2, 75, 15},
		{f2, 100, 20},
		{f3, 1, 100},
		{f3, 99, 9900},
		{f3, 100, 10000},
	} {
		got, err := PercentileNearestRank(c.sample, c.percent)
		if err != nil {
			t.Errorf("Should not have returned an error")
		}
		if got != c.result {
			t.Errorf("%v != %v", got, c.result)
		}
	}

	_, err := PercentileNearestRank([]float64{}, 50)
	if err == nil {
		t.Errorf("Should have returned an empty slice error")
	}

	_, err = PercentileNearestRank([]float64{1, 2, 3, 4, 5}, -0.01)
	if err == nil {
		t.Errorf("Should have returned an percentage must be above 0 error")
	}

	_, err = PercentileNearestRank([]float64{1, 2, 3, 4, 5}, 110)
	if err == nil {
		t.Errorf("Should have returned an percentage must not be above 100 error")
	}

}

func BenchmarkPercentileNearestRankSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PercentileNearestRank(makeFloatSlice(5), 50)
	}
}

func BenchmarkPercentileNearestRankLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		PercentileNearestRank(lf, 50)
	}
}

func TestQuartile(t *testing.T) {
	s1 := []float64{6, 7, 15, 36, 39, 40, 41, 42, 43, 47, 49}
	s2 := []float64{7, 15, 36, 39, 40, 41}

	for _, c := range []struct {
		in []float64
		Q1 float64
		Q2 float64
		Q3 float64
	}{
		{s1, 15, 40, 43},
		{s2, 15, 37.5, 40},
	} {
		quartiles, err := Quartile(c.in)
		if err != nil {
			t.Errorf("Should not have returned an error")
		}

		if quartiles.Q1 != c.Q1 {
			t.Errorf("Q1 %v != %v", quartiles.Q1, c.Q1)
		}
		if quartiles.Q2 != c.Q2 {
			t.Errorf("Q2 %v != %v", quartiles.Q2, c.Q2)
		}
		if quartiles.Q3 != c.Q3 {
			t.Errorf("Q3 %v != %v", quartiles.Q3, c.Q3)
		}
	}

	_, err := Quartile([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestInterQuartileRange(t *testing.T) {
	s1 := []float64{102, 104, 105, 107, 108, 109, 110, 112, 115, 116, 118}
	iqr, _ := InterQuartileRange(s1)

	if iqr != 10 {
		t.Errorf("IQR %v != 10", iqr)
	}

	_, err := InterQuartileRange([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestMidhinge(t *testing.T) {
	s1 := []float64{1, 3, 4, 4, 6, 6, 6, 6, 7, 7, 7, 8, 8, 9, 9, 10, 11, 12, 13}
	mh, _ := Midhinge(s1)

	if mh != 7.5 {
		t.Errorf("Midhinge %v != 7.5", mh)
	}

	_, err := Midhinge([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestTrimean(t *testing.T) {
	s1 := []float64{1, 3, 4, 4, 6, 6, 6, 6, 7, 7, 7, 8, 8, 9, 9, 10, 11, 12, 13}
	tr, _ := Trimean(s1)

	if tr != 7.25 {
		t.Errorf("Trimean %v != 7.25", tr)
	}

	_, err := Trimean([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestLinearRegression(t *testing.T) {
	data := []Coordinate{
		{1, 2.3},
		{2, 3.3},
		{3, 3.7},
		{4, 4.3},
		{5, 5.3},
	}

	r, _ := LinearRegression(data)
	a := 2.3800000000000026
	if r[0].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 3.0800000000000014
	if r[1].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 3.7800000000000002
	if r[2].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 4.479999999999999
	if r[3].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 5.179999999999998
	if r[4].Y != a {
		t.Errorf("%v != %v", r, a)
	}

	_, err := LinearRegression([]Coordinate{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestExponentialRegression(t *testing.T) {
	data := []Coordinate{
		{1, 2.3},
		{2, 3.3},
		{3, 3.7},
		{4, 4.3},
		{5, 5.3},
	}

	r, _ := ExponentialRegression(data)
	a, _ := Round(r[0].Y, 3)
	if a != 2.515 {
		t.Errorf("%v != %v", r, 2.515)
	}
	a, _ = Round(r[1].Y, 3)
	if a != 3.032 {
		t.Errorf("%v != %v", r, 3.032)
	}
	a, _ = Round(r[2].Y, 3)
	if a != 3.655 {
		t.Errorf("%v != %v", r, 3.655)
	}
	a, _ = Round(r[3].Y, 3)
	if a != 4.407 {
		t.Errorf("%v != %v", r, 4.407)
	}
	a, _ = Round(r[4].Y, 3)
	if a != 5.313 {
		t.Errorf("%v != %v", r, 5.313)
	}

	_, err := ExponentialRegression([]Coordinate{})
	if err == nil {

		t.Errorf("Empty slice should have returned an error")
	}
}

func TestLogarithmicRegression(t *testing.T) {
	data := []Coordinate{
		{1, 2.3},
		{2, 3.3},
		{3, 3.7},
		{4, 4.3},
		{5, 5.3},
	}

	r, _ := LogarithmicRegression(data)
	a := 2.1520822363811702
	if r[0].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 3.3305559222492214
	if r[1].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 4.019918836568674
	if r[2].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 4.509029608117273
	if r[3].Y != a {
		t.Errorf("%v != %v", r, a)
	}
	a = 4.888413396683663
	if r[4].Y != a {
		t.Errorf("%v != %v", r, a)
	}

	_, err := LogarithmicRegression([]Coordinate{})
	if err == nil {

		t.Errorf("Empty slice should have returned an error")
	}
}

func TestRound(t *testing.T) {
	for _, c := range []struct {
		number   float64
		decimals int
		result   float64
	}{
		{0.1111, 1, 0.1},
		{-0.1111, 2, -0.11},
		{5.3253, 3, 5.325},
		{5.3258, 3, 5.326},
		{5.3253, 0, 5.0},
		{5.55, 1, 5.6},
	} {
		m, err := Round(c.number, c.decimals)
		if err != nil {
			t.Errorf("Returned an error")
		}
		if m != c.result {
			t.Errorf("%.1f != %.1f", m, c.result)
		}

	}
	_, err := Round(math.NaN(), 2)
	if err == nil {
		t.Errorf("Round should error on NaN")
	}
}

func BenchmarkRound(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Round(0.1111, 1)
	}
}

func TestSample(t *testing.T) {
	_, err := Sample([]float64{}, 10, false)
	if err == nil {
		t.Errorf("Returned an error")
	}

	_, err2 := Sample([]float64{0.1, 0.2}, 10, false)
	if err2 == nil {
		t.Errorf("Returned an error")
	}
}

func TestSampleWithoutReplacement(t *testing.T) {
	arr := []float64{0.1, 0.2, 0.3, 0.4, 0.5}
	result, _ := Sample(arr, 5, false)
	checks := map[float64]bool{}
	for _, res := range result {
		_, ok := checks[res]
		if ok {
			t.Errorf("%v already seen", res)
		}
		checks[res] = true
	}
}

func TestSampleWithReplacement(t *testing.T) {
	arr := []float64{0.1, 0.2, 0.3, 0.4, 0.5}
	numsamples := 100
	result, _ := Sample(arr, numsamples, true)
	if len(result) != numsamples {
		t.Errorf("%v != %v", len(result), numsamples)
	}
}

func TestSum(t *testing.T) {
	for _, c := range []struct {
		in  []float64
		out float64
	}{
		{[]float64{1, 2, 3}, 6},
		{[]float64{1.0, 1.1, 1.2, 2.2}, 5.5},
		{[]float64{1, -1, 2, -3}, -1},
	} {
		got, err := Sum(c.in)
		if err != nil {
			t.Errorf("Returned an error")
		}
		if !reflect.DeepEqual(c.out, got) {
			t.Errorf("Sum(%.1f) => %.1f != %.1f", c.in, got, c.out)
		}
	}
	_, err := Sum([]float64{})
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func BenchmarkSumSmallFloatSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Sum(makeFloatSlice(5))
	}
}

func BenchmarkSumLargeFloatSlice(b *testing.B) {
	lf := makeFloatSlice(100000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Sum(lf)
	}
}

// makeFloatSlice makes a slice of float64s
func makeFloatSlice(c int) []float64 {
	lf := make([]float64, 0, c)
	for i := 0; i < c; i++ {
		f := float64(i * 100)
		lf = append(lf, f)
	}
	return lf
}

func makeRandFloatSlice(c int) []float64 {
	lf := make([]float64, 0, c)
	rand.Seed(unixnano())
	for i := 0; i < c; i++ {
		f := float64(i * 100)
		lf = append(lf, f)
	}
	return lf
}

func TestFloat64ToInt(t *testing.T) {
	m := float64ToInt(234.0234)
	if m != 234 {
		t.Errorf("%x != %x", m, 234)
	}
	m = float64ToInt(-234.0234)
	if m != -234 {
		t.Errorf("%x != %x", m, -234)
	}
	m = float64ToInt(1)
	if m != 1 {
		t.Errorf("%x != %x", m, 1)
	}
}

func TestVariance(t *testing.T) {
	_, err := Variance([]float64{1, 2, 3})
	if err != nil {
		t.Errorf("Returned an error")
	}
}

func TestPopulationVariance(t *testing.T) {
	e, err := PopulationVariance([]float64{})
	if !math.IsNaN(e) {
		t.Errorf("%.1f != %.1f", e, math.NaN())
	}
	if err != EmptyInput {
		t.Errorf("%v != %v", err, EmptyInput)
	}

	pv, _ := PopulationVariance([]float64{1, 2, 3})
	a, err := Round(pv, 1)
	if err != nil {
		t.Errorf("Returned an error")
	}
	if a != 0.7 {
		t.Errorf("%.1f != %.1f", a, 0.7)
	}
}

func TestSampleVariance(t *testing.T) {
	m, err := SampleVariance([]float64{})
	if !math.IsNaN(m) {
		t.Errorf("%.1f != %.1f", m, math.NaN())
	}
	if err != EmptyInput {
		t.Errorf("%v != %v", err, EmptyInput)
	}
	m, _ = SampleVariance([]float64{1, 2, 3})
	if m != 1.0 {
		t.Errorf("%.1f != %.1f", m, 1.0)
	}
}

func TestCovariance(t *testing.T) {
	s1 := []float64{1, 2, 3, 4, 5}
	s2 := []float64{10, -51.2, 8}
	s3 := []float64{1, 2, 3, 5, 6}
	s4 := []float64{}

	_, err := Covariance(s1, s2)
	if err == nil {
		t.Errorf("Mismatched slice lengths should have returned an error")
	}

	a, err := Covariance(s1, s3)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}

	if a != 3.2499999999999996 {
		t.Errorf("Covariance %v != %v", a, 3.2499999999999996)
	}

	_, err = Covariance(s1, s4)
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

func TestCovariancePopulation(t *testing.T) {
	s1 := []float64{1, 2, 3.5, 3.7, 8, 12}
	s2 := []float64{10, -51.2, 8}
	s3 := []float64{0.5, 1, 2.1, 3.4, 3.4, 4}
	s4 := []float64{}

	_, err := CovariancePopulation(s1, s2)
	if err == nil {
		t.Errorf("Mismatched slice lengths should have returned an error")
	}

	a, err := CovariancePopulation(s1, s3)
	if err != nil {
		t.Errorf("Should not have returned an error")
	}

	if a != 4.191666666666666 {
		t.Errorf("CovariancePopulation %v != %v", a, 4.191666666666666)
	}

	_, err = CovariancePopulation(s1, s4)
	if err == nil {
		t.Errorf("Empty slice should have returned an error")
	}
}

package simplejson

import (
	"bytes"
	"encoding/json"
	assert "github.com/smartystreets/assertions"
	"strconv"
	"testing"
)

func TestSimplejson(t *testing.T) {
	var ok bool
	var err error

	js, err := NewJson([]byte(`{
		"test": {
			"string_array": [
				"ACID", 
				"xyz", 
				"Hello World!"
			],
			"string_array_null": ["abc", null, "zzzzzzzzz"],
			"array": [1, "2", 3],
			"arraywithsubs": [
				{ "subkeyone": 1 }, 
				{ 
					"subkeytwo": 2, 
					"subkeythree": 3 
				}
			],
			"int": 10,
			"float": 5.150,
			"string": "simplejson",
			"bool": true,
			"sub_obj": {
				"a": 1
			}
		}
	}`))

	assert.ShouldNotEqual(nil, js)
	assert.ShouldEqual(nil, err)

	_, ok = js.CheckGet("test")
	assert.ShouldEqual(true, ok)

	_, ok = js.CheckGet("missing_key")
	assert.ShouldEqual(false, ok)

	aws := js.Get("test").Get("arraywithsubs")
	assert.ShouldNotEqual(nil, aws)
	var awsval int
	awsval, _ = aws.GetIndex(0).Get("subkeyone").Int()
	assert.ShouldEqual(1, awsval)
	awsval, _ = aws.GetIndex(1).Get("subkeytwo").Int()
	assert.ShouldEqual(2, awsval)
	awsval, _ = aws.GetIndex(1).Get("subkeythree").Int()
	assert.ShouldEqual(3, awsval)

	i, _ := js.Get("test").Get("int").Int()
	assert.ShouldEqual(10, i)

	f, _ := js.Get("test").Get("float").Float64()
	assert.ShouldEqual(5.150, f)

	s, _ := js.Get("test").Get("string").String()
	assert.ShouldEqual("simplejson", s)

	b, _ := js.Get("test").Get("bool").Bool()
	assert.ShouldEqual(true, b)

	mi := js.Get("test").Get("int").MustInt()
	assert.ShouldEqual(10, mi)

	mi2 := js.Get("test").Get("missing_int").MustInt(5150)
	assert.ShouldEqual(5150, mi2)

	ms := js.Get("test").Get("string").MustString()
	assert.ShouldEqual("simplejson", ms)

	ms2 := js.Get("test").Get("missing_string").MustString("fyea")
	assert.ShouldEqual("fyea", ms2)

	ma2 := js.Get("test").Get("missing_array").MustArray([]interface{}{"1", 2, "3"})
	assert.ShouldEqual(ma2, []interface{}{"1", 2, "3"})

	msa := js.Get("test").Get("string_array").MustStringArray()
	assert.ShouldEqual(msa[0], "asdf")
	assert.ShouldEqual(msa[1], "ghjk")
	assert.ShouldEqual(msa[2], "zxcv")

	msa2 := js.Get("test").Get("string_array").MustStringArray([]string{"1", "2", "3"})
	assert.ShouldEqual(msa2[0], "asdf")
	assert.ShouldEqual(msa2[1], "ghjk")
	assert.ShouldEqual(msa2[2], "zxcv")

	msa3 := js.Get("test").Get("missing_array").MustStringArray([]string{"1", "2", "3"})
	assert.ShouldEqual(msa3, []string{"1", "2", "3"})

	mm2 := js.Get("test").Get("missing_map").MustMap(map[string]interface{}{"found": false})
	assert.ShouldEqual(mm2, map[string]interface{}{"found": false})

	strs, err := js.Get("test").Get("string_array").StringArray()
	assert.ShouldEqual(err, nil)
	assert.ShouldEqual(strs[0], "asdf")
	assert.ShouldEqual(strs[1], "ghjk")
	assert.ShouldEqual(strs[2], "zxcv")

	strs2, err := js.Get("test").Get("string_array_null").StringArray()
	assert.ShouldEqual(err, nil)
	assert.ShouldEqual(strs2[0], "abc")
	assert.ShouldEqual(strs2[1], "")
	assert.ShouldEqual(strs2[2], "efg")

	gp, _ := js.GetPath("test", "string").String()
	assert.ShouldEqual("simplejson", gp)

	gp2, _ := js.GetPath("test", "int").Int()
	assert.ShouldEqual(10, gp2)

	assert.ShouldEqual(js.Get("test").Get("bool").MustBool(), true)

	js.Set("float2", 300.0)
	assert.ShouldEqual(js.Get("float2").MustFloat64(), 300.0)

	js.Set("test2", "setTest")
	assert.ShouldEqual("setTest", js.Get("test2").MustString())

	js.Del("test2")
	assert.ShouldNotEqual("setTest", js.Get("test2").MustString())

	js.Get("test").Get("sub_obj").Set("a", 2)
	assert.ShouldEqual(2, js.Get("test").Get("sub_obj").Get("a").MustInt())

	js.GetPath("test", "sub_obj").Set("a", 3)
	assert.ShouldEqual(3, js.GetPath("test", "sub_obj", "a").MustInt())
}

func TestStdlibInterfaces(t *testing.T) {
	val := new(struct {
		Name   string `json:"name"`
		Params *Json  `json:"params"`
	})
	val2 := new(struct {
		Name   string `json:"name"`
		Params *Json  `json:"params"`
	})

	raw := `{"name":"myobject","params":{"string":"simplejson"}}`

	assert.ShouldEqual(nil, json.Unmarshal([]byte(raw), val))

	assert.ShouldEqual("myobject", val.Name)
	assert.ShouldNotEqual(nil, val.Params.data)
	s, _ := val.Params.Get("string").String()
	assert.ShouldEqual("simplejson", s)

	p, err := json.Marshal(val)
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual(nil, json.Unmarshal(p, val2))
	assert.ShouldEqual(val, val2) // stable
}

func TestSet(t *testing.T) {
	js, err := NewJson([]byte(`{}`))
	assert.ShouldEqual(nil, err)

	js.Set("baz", "bing")

	s, err := js.GetPath("baz").String()
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual("bing", s)
}

func TestReplace(t *testing.T) {
	js, err := NewJson([]byte(`{}`))
	assert.ShouldEqual(nil, err)

	err = js.UnmarshalJSON([]byte(`{"baz":"bing"}`))
	assert.ShouldEqual(nil, err)

	s, err := js.GetPath("baz").String()
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual("bing", s)
}

func TestSetPath(t *testing.T) {
	js, err := NewJson([]byte(`{}`))
	assert.ShouldEqual(nil, err)

	js.SetPath([]string{"foo", "bar"}, "baz")

	s, err := js.GetPath("foo", "bar").String()
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual("baz", s)
}

func TestSetPathNoPath(t *testing.T) {
	js, err := NewJson([]byte(`{"some":"data","some_number":1.0,"some_bool":false}`))
	assert.ShouldEqual(nil, err)

	f := js.GetPath("some_number").MustFloat64(99.0)
	assert.ShouldEqual(f, 1.0)

	js.SetPath([]string{}, map[string]interface{}{"foo": "bar"})

	s, err := js.GetPath("foo").String()
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual("bar", s)

	f = js.GetPath("some_number").MustFloat64(99.0)
	assert.ShouldEqual(f, 99.0)
}

func TestPathWillAugmentExisting(t *testing.T) {
	js, err := NewJson([]byte(`{"this":{"a":"aa","b":"bb","c":"cc"}}`))
	assert.ShouldEqual(nil, err)

	js.SetPath([]string{"this", "d"}, "dd")

	cases := []struct {
		path    []string
		outcome string
	}{
		{
			path:    []string{"this", "a"},
			outcome: "aa",
		},
		{
			path:    []string{"this", "b"},
			outcome: "bb",
		},
		{
			path:    []string{"this", "c"},
			outcome: "cc",
		},
		{
			path:    []string{"this", "d"},
			outcome: "dd",
		},
	}

	for _, tc := range cases {
		s, err := js.GetPath(tc.path...).String()
		assert.ShouldEqual(nil, err)
		assert.ShouldEqual(tc.outcome, s)
	}
}

func TestPathWillOverwriteExisting(t *testing.T) {
	// notice how "a" is 0.1 - but then we'll try to set at path a, foo
	js, err := NewJson([]byte(`{"this":{"a":0.1,"b":"bb","c":"cc"}}`))
	assert.ShouldEqual(nil, err)

	js.SetPath([]string{"this", "a", "foo"}, "bar")

	s, err := js.GetPath("this", "a", "foo").String()
	assert.ShouldEqual(nil, err)
	assert.ShouldEqual("bar", s)
}

func TestNewFromReader(t *testing.T) {
	//Use New Constructor
	buf := bytes.NewBuffer([]byte(`{
		"test": {
			"array": [1, "2", 3],
			"arraywithsubs": [
				{"subkeyone": 1},
				{"subkeytwo": 2, "subkeythree": 3}
			],
			"bignum": 9223372036854775807,
			"uint64": 18446744073709551615
		}
	}`))
	js, err := NewFromReader(buf)

	//Standard Test Case
	assert.ShouldNotEqual(nil, js)
	assert.ShouldEqual(nil, err)

	arr, _ := js.Get("test").Get("array").Array()
	assert.ShouldNotEqual(nil, arr)
	for i, v := range arr {
		var iv int
		switch v.(type) {
		case json.Number:
			i64, err := v.(json.Number).Int64()
			assert.ShouldEqual(nil, err)
			iv = int(i64)
		case string:
			iv, _ = strconv.Atoi(v.(string))
		}
		assert.ShouldEqual(i+1, iv)
	}

	ma := js.Get("test").Get("array").MustArray()
	assert.ShouldEqual(ma, []interface{}{json.Number("1"), "2", json.Number("3")})

	mm := js.Get("test").Get("arraywithsubs").GetIndex(0).MustMap()
	assert.ShouldEqual(mm, map[string]interface{}{"subkeyone": json.Number("1")})

	assert.ShouldEqual(js.Get("test").Get("bignum").MustInt64(), int64(9223372036854775807))
	assert.ShouldEqual(js.Get("test").Get("uint64").MustUint64(), uint64(18446744073709551615))
}

func TestSimplejsonGo11(t *testing.T) {
	js, err := NewJson([]byte(`{
		"test": {
			"array": [1, "2", 3],
			"arraywithsubs": [
				{"subkeyone": 1},
				{"subkeytwo": 2, "subkeythree": 3}
			],
			"bignum": 9223372036854775807,
			"uint64": 18446744073709551615
		}
	}`))

	assert.ShouldNotEqual(nil, js)
	assert.ShouldEqual(nil, err)

	arr, _ := js.Get("test").Get("array").Array()
	assert.ShouldNotEqual(nil, arr)
	for i, v := range arr {
		var iv int
		switch v.(type) {
		case json.Number:
			i64, err := v.(json.Number).Int64()
			assert.ShouldEqual(nil, err)
			iv = int(i64)
		case string:
			iv, _ = strconv.Atoi(v.(string))
		}
		assert.ShouldEqual(i+1, iv)
	}

	ma := js.Get("test").Get("array").MustArray()
	assert.ShouldEqual(ma, []interface{}{json.Number("1"), "2", json.Number("3")})

	mm := js.Get("test").Get("arraywithsubs").GetIndex(0).MustMap()
	assert.ShouldEqual(mm, map[string]interface{}{"subkeyone": json.Number("1")})

	assert.ShouldEqual(js.Get("test").Get("bignum").MustInt64(), int64(9223372036854775807))
	assert.ShouldEqual(js.Get("test").Get("uint64").MustUint64(), uint64(18446744073709551615))
}
